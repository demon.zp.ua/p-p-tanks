import store from '../../store';
import {on} from '../../services/socketServices'; 

export const SocketRoomEvents = (socket)=>{
    on(socket,'update_rooms',data=>{
        store.commit('rooms/setRooms', data);
        //console.log('soket connect-to-room = ', data);
    });

    on(socket,'update_room',data=>{
        console.log('поймал апдейт комнаты');
        store.commit('rooms/updateRoom', data);
        //console.log('soket connect-to-room = ', data);
    });

    on(socket,'join_room',data=>{
        store.commit('rooms/updateRoom', data);
    });
   
    on(socket,'start_game',data=>{
        store.commit('rooms/updateRoom', null);
        store.commit('game/isGame', true);
        //console.log('soket start-game = ', data);
    });
}