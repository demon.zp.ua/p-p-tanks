import store from '../../store';
import {on} from '../../services/socketServices';

export const SocketEvents = (socket)=>{
    on(socket,'disconnect',(reason) => {
        console.log('disconnect = ', reason);
        store.state.user.is_connect = false;
        store.commit('user/delUser');
        store.commit('game/delGame');
        //socket.open();
        //off(socket);
        //console.log('store = ', store);
    });

    on(socket,'is_connect',(data)=>{
        console.log('есть подключение!!');
    });
    
    on(socket,'connect_error',(error)=>{
        console.log('connect_error = ', error);
        socket.close();
    });
   
    on(socket,'connect',()=> {
        store.state.user.is_connect = true;
        console.log('подключился!!!!');
    });

    on(socket,'join',(response)=>{
        if('errors' in response){
            console.log("Ошибка!!! = ", response.errors);
            return;
        }
        store.commit('user/setUser', response.data);
        console.log('успешно законектился');
    });
}