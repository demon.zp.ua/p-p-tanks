import store from '../../store';
import {on} from '../../services/socketServices'; 

export const SocketGameEvents = (socket)=>{
    on(socket,'init_game',data=>{
        console.log('soket game_init = ', data);
        store.commit('game/init', data);
    });
    on(socket,'game_start',data=>{
        console.log('soket game_start = ', data);
    });
    /*socket.on('connect-to-game',data=>{
        console.log('soket connect-to-room = ', data);
    });*/
    on(socket,'game_update',data=>{
        //console.log('soket game_update = ', data);
        store.commit('game/updateGame', data);
        
    });

    on(socket,'game_add_bullet',data=>{
        console.log('должен добавить в очередь на рендер Пулю!!!');
        //console.log('soket game_update = ', data);
        store.commit('game/addBullet', data);
        
    });
    
    on(socket,'game_del_bullet',data=>{
        console.log('должен добавить в очередь удалить Пулю!!! = ',data);
        //console.log('soket game_update = ', data);
        store.commit('game/delBullet', data);
        
    });

    on(socket,'game_over',data=>{
        console.log('data = ', data);
        store.commit('game/updateGame', data);
        //store.commit('game/delGame');
    });
    /*socket.on('start-game',data=>{
        console.log('soket start-game = ', data);
    });*/
    //console.log('socket = ', socket);
}