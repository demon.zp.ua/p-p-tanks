class KeyEvents{
    constructor(){
        this._events_down = {};
        this._events_up = {};
        document.addEventListener('keydown', this.onKeyDown.bind(this));
        document.addEventListener('keyup', this.onKeyUp.bind(this));
    }

    add({key_code,e,func,context}){
        if(e==='down'){
            this._events_down[key_code] = func.bind(context);
        }else{
            this._events_up[key_code] = func.bind(context);
        }
    }

    del(key_code){
        delete this._events_down[key_code];
        delete this._events_up[key_code];
    }

    delAll(arr){
        arr.forEach(key => {
            delete this._events_down[key];
            delete this._events_up[key];
        });
    }

    onKeyDown(e){
        //console.log('жмакнул = ', e.code);
        if(this._events_down.hasOwnProperty(e.code)){
            this._events_down[e.code]();
        }
    }

    onKeyUp(e){
        if(this._events_up.hasOwnProperty(e.code)){
            this._events_up[e.code]();
        }
    }
}

const keyEvents = new KeyEvents();

export default keyEvents;
