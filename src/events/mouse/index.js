export default class Mouser{
    constructor(el,func,context){
        this.el = el;
        this.func = func.bind(context);
        this.el.addEventListener('mousemove', this.onMove.bind(this));
    }

    onMove(e){
        this.func({x:e.clientX,y:e.clientY});
    }
}