export const on = (sock, event, func)=>{
    const socket = sock.socket;
    if(socket.hasOwnProperty('_callbacks') && socket._callbacks.hasOwnProperty(`$${event}`)){
        console.log('уже слушаю = ', event);
        return ;
    }
    console.log('создал слушатель = ', event);
    socket.on(event, func);
}

import io from 'socket.io-client';
import {SocketEvents} from '../events/socket/index';

export const socket = {
    socket: null,
    emit(ev, payload){
        this.socket.emit(ev, payload);
    },
    connect(){
        this.socket = io();
        //this.socket = io('http://localhost:3000');
        SocketEvents(socket);
        //return sock;
    }
    //const sock = io();

}

// export const socket = ()=>{
//     //const sock = io();
//     const sock = io('http://localhost:3000');
//     SocketEvents(sock);
//     return sock;
// }