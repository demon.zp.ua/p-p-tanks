const getAngle = ({tank,mouse,angle})=>{
    const mDx2 = tank.x - mouse.x;
    const mDy2 = tank.y - mouse.y;
    const mAngle = Math.atan2(mDy2, mDx2);
	//получаем угол между мышкой и башней в градусах
    const mAngle2 = mAngle / Math.PI * 180;
    const dAngle = angle - mAngle2;
    return dAngle;
}
export default getAngle;