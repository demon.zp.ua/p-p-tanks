export default class ScaleService{
    static getSize(w,h){

        let razn = 0;
        let max = w;
        let min = h;

        if(w<h){
            max = h;
            min = w;
        }

        razn = max/min;

        if(razn>2){
            min = Math.floor(max/1.77);
        }

        return {w:max,h:min};
    }

    static getScale(w,scale){
        let koefic = 0;
        let t_koefic = 0;

        if(w>888){
            t_koefic = 888/w;
            t_koefic = String(t_koefic).slice(0,4);
            //console.log('t_koefic > ',t_koefic);
            koefic = scale+(1 - t_koefic);
        }else{
            t_koefic = w/888;
            t_koefic = String(t_koefic).slice(0,4);
            //console.log('t_koefic < ',t_koefic);
            koefic = scale-(1 - t_koefic);
        }

        //console.log('koefic = ',koefic);

        return koefic;
    }
}