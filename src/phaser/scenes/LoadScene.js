/*
Класс загрузки ассетов для игры, картинок карт использует класс general
где генерируються имена карт для типа удобной загрузки картинок через цикл
*/

import Phaser from '../../phaser.min';
import store from '../../store';

export default class LoadScene extends Phaser.Scene{
	constructor(config){
        //super(config);
        super({key:"LoadScene"});
        this.timer = null;
    }

    preload(){
        const loadingBar = this.add.graphics({
			fillStyle:{
				color:0xffffff
			}
        });
        
        this.load.on("progress",(percent)=>{
			loadingBar.fillRect(0,this.game.renderer.height/2,this.game.renderer.width*percent,50);
		});
		
		this.load.on("complete", ()=>{
            console.log("загрузка завершена!!!!!!!!!!!!!!");
            this.goGame();
        });

        this.load.image('body', './assets/body+gusli2.png');
        this.load.image('tower', './assets/tower+gun2.png');
        this.load.image('wall', './assets/wall.png');
        this.load.image('bullet1', './assets/bullet1.png');
        this.load.image('smog', './assets/smog.png');
        //this.load.image('lvl_de_dust', './assets/lvl_de_dust.png');
        this.load.image('lvl_de_dust', './assets/lvl_de_dust_2.jpg');
        //this.isReady();
    }

	myPreload(){
        console.log('должен грузить ассеты!!!! = ', this.load);
        this.load.image('body', './assets/body+gusli.png');
        this.load.image('tower', './assets/tower+gun.png');
        this.load.image('wall', './assets/wall.png');
        
    }

    isReady(){
        if(Object.keys(store.state.game.game).length<=0){
            console.log('ожидаю сервер!');
            clearTimeout(this.timer);
            this.timer = setTimeout(this.isReady.bind(this),60);
        }else{
            if(store.state.game.game.status==='init'){
                console.log('ожидаю сервер!');
                clearTimeout(this.timer);
                this.timer = setTimeout(this.isReady.bind(this),60);
            }else{
                console.log('сервер готов!!!');
                clearTimeout(this.timer);
                this.myPreload();
            }
        }
    }

    goGame(){
        this.scene.start("GameScene");
    }

// после загрузки всех ассетов стартует GameScene
	// create(data){
    //     console.log('стартую сцену!!!');
	// 	this.scene.start("GameScene");
	// 	//this.scene.start("ServerScene");
	// }
}