// Класс игровой сцены содержин визуальную логику игры
import Phaser from "../../phaser.min";
import store from "../../store";
import Player from "../objects/Player";
import PlayerOfline from "../objects/Player_ofline";
import WallOfline from "../objects/WallOfline";
import WallAngleOfl from "../objects/WallAngleOfl";
import Wall from "../objects/Wall";
import Enemys from "../objects/Enemys";
import Bullets from "../objects/Bullets";
import vue from "../../main";
import TimeOute from "../objects/TimeOute";

export default class GameScene extends Phaser.Scene{
	constructor(config){
        super({key:"GameScene"});
        //console.log('config.socket = ',config);
        //this.socket = config.socket;
        //super(config);
        this._d_width = 888;
        this._d_height = 600;
		this.width_lvl = 980;
        this.height_lvl = 800;
        this._scale = 0.3;
        this.gg = null;
        this.enemys = null;
        this.bullets = null;
        this.tanks = {};
        this.functions = [];
        this.map_walls = [
            {x:200,y:200},
            {x:200,y:240},
            {x:200,y:280},
            {x:320,y:200},
            {x:360,y:200},
            {x:400,y:200},
            {x:40,y:450},
            {x:60,y:450},
            {x:400,y:600},
            {x:400,y:640},
            {x:400,y:680},
            {x:240,y:470},
            {x:240,y:510},
            {x:640,y:520},
            {x:640,y:560},
            {x:600,y:400},
            {x:620,y:400},
            {x:660,y:400},
            {x:700,y:400},
            {x:740,y:400},
            {x:400,y:420},
            {x:540,y:200},
            {x:580,y:200},
            {x:580,y:240},
            {x:400,y:40},
            {x:400,y:80},
            {x:680,y:100},
            {x:90,y:280}
        ];
        this.walls = [];
        this.pointer = {x:0,y:0};
        
        this.is_cam_free = false;
        //this.scale = 1;
	}

// Создает класс Игровой логики клиентской части
	init(){
		
		//this.gameLogic = new GameLogic(this);
		//vue.$store.commit('socket/setGameLogic', this);
	}

    forceBattle(){
        this.gg.forceBattle();
        this.enemys.forceBattle();
    }

	create(){
        console.log("создал сцену Гаме", this);
        this.cameras.main.setBounds(-40, -50, this.width_lvl, this.height_lvl);
        let map = this.add.image(460, 350, 'lvl_de_dust');
        this.gg = new Player(this);

        this.enemys = new Enemys(this);

        this.bullets = new Bullets(this);

        this.map_walls.forEach((wall,i)=>{
            this.walls.push(new Wall(
                {
                    ...wall,
                    scene:this,
                    i
                }
            ));
        });
        
        // let wall = new WallOfline({
        //     x:200,
        //     y:200,
        //     scene:this
        // });

        // let wall = new Wall({
        //     x:200,
        //     y:200,
        //     angle:30,
        //     scene:this
        // });

        // this.walls.push(wall);

        
        // this.walls.push(
        //     this.add.sprite(200, 200, 'wall')
        //     .setDisplaySize(40,40)
        // );

        this.camOnObj(this.gg.tank.cont);

        

        this.input.on("pointermove",function(pointer){
			this.pointer.x = pointer.worldX;
            this.pointer.y = pointer.worldY;
            // vue.$socket.emit('mouse_move',getAngle(
            //     {
            //         tank:store.getters['game/getTankPos'],
            //         mouse:this.pointer,
            //         angle:store.getters['game/getTowerAngle']
            //     }
            // ));
            vue.$socket.emit('mouse_move',this.pointer);
        },this);
        
        this.input.on('pointerdown', function (pointer) {
			//this.pointer.worldX = pointer.worldX;
            //this.pointer.worldY = pointer.worldY;
            console.log('выстрел!!!');
            vue.$socket.emit('fire',{});
        },this);

        if(store.state.game.game.status==='battle'){
            this.forceBattle();
        }
        vue.$store.commit('game/isGameScene',true);
        const timeOute = new TimeOute(this);
        this.functions.push({key:'timeOut', fn:timeOute.update.bind(timeOute)});
	}

    delTimeOut(){
        //console.log('delTimeOut');
        this.functions = this.functions.filter(({key})=>key!=='timeOut');
    }

    camOnObj(obj){
		this.is_cam_free = false;
		this.cameras.main.startFollow(obj);
		//this.index.cameras.main.setPosition(500,this.obj.y);
        this.cameras.main.followOffset.set(0, 0);
        console.log('камера должна навестись на гг');
    }

    onResize({w,h}){
        console.log('установить = ', w,'||',h);
        this.scale.resize(w, h);
        this.scale.updateBounds();
    }
    
	render(){
        
    }

    del(){
        this.gg.del();
    }

	update(_,delta){
        if(Object.keys(store.state.game.game).length>0){
            if(this.gg.ready_to_del){
                store.commit('game/delGame');
                //this.scene.destroy();
                return;
            }
            this.functions.forEach(el=>el.fn(delta));
            this.gg.update();
            this.enemys.update();
            this.bullets.update();
            if(store.state.game.game.status==='game_over'){
                //console.log('this = ',this);
                this.scene.pause();
            }
        }
        //this.cameras.main.scrollX -= 5;
        //this.cameras.main.scrollX += 5;
        //this.sprite.x +=1;
		//console.log('time = ', store.getters['game/getTimeToStart']);
	}
}