import Body from './Body';
import Tower from './Tower';
import HpBar from './HpBar';

export default class Tank{
    constructor({scene,id,x,y,angle,side}){
        this.scene = scene;
        this.id = id;
        this.cont = null;
        this.body = null;
        this.tower = null;
        this.smog = null;
        this.hp_bar = null;
        this.hit_points = 1000;
        this.x = x;
        this.y = y;
        this.posX = x;
        this.rot = angle;
        this.side = side;
        this.render();
        this.p_sX = { min: 0, max: 0 };
    }

    render(){
        this.cont = this.scene.add.container(this.x, this.y)
        .setDepth(2);

        this.body = new Body(this.scene);
        this.tower = new Tower(this.scene);
        if(this.side==='enemy'){
            this.hp_bar = new HpBar(this.scene,this.side);
        }
        const particles = this.scene.add.particles('smog');
        //this.smog = this.scene.add.particles('smog');
        this.smog = particles.createEmitter({
            x: 16,
            y: {min:0-7,max:0+7},
            lifespan: 1000,
            speedX: { min: 10, max: 20 },
            speedY: { min: -10, max: 10 },
            scale: { start: 0.02, end: 0.1 },
            alpha:{start:1,end:0.4},
            angle:60,
            quantity: 1,
            //blendMode: 'ADD'
        });
        //console.log('smog = ', this.smog.killAll);
        this.smog.stop();

        this.cont.add([this.body.sprite, particles, this.tower.cont]);
        if(this.hp_bar){
            this.cont.add(this.hp_bar.cont);
        }
        this.cont.angle = this.rot;
        
        //console.log('танк = ',this);
        // setTimeout(()=>{
        //     console.log('тут!! = ',this.smog);
        //     //this.smog.lifespan = 800;
        //     this.smog.setSpeedX({ min: 50, max: 150 })
        // }, 2000);
        //this.scene.matter.add.gameObject(this.cont);
    }

    forceBattle(){
        console.log('Tank forceBattle');
        if(this.hit_points>0){
            this.smog.start();
        }
    }

    update({x,y,angle,speed_rot,speed,tower_angle,percent_hp,hit_points}){
        //this.posX+=1;
        this.hit_points = hit_points;
        this.cont.x = x;
        this.cont.y = y;
        let quantity = 1;
        let correct = 1;
        let p_sX = {min:10,max:20};
        if(hit_points>0){
            if(speed>0){
                correct = -1;
                p_sX.min = -30;
                p_sX.max = -50;
                quantity = 3;
            }
    
            if(speed<0){
                p_sX.min = 30;
                p_sX.max = 50;
                quantity = 3;
            }
            //this.smog.setPosition(this.cont.x, this.cont.y);
            if(speed_rot>0){
                if(correct==-1){
                    p_sX.min -= 5;
                    p_sX.max -= 10;
                }else{
                    p_sX.min += 5;
                    p_sX.max += 10;
                }
                //this.smog.setSpeedX({ min: this.p_sX.min+20, max: this.p_sX.max+50 });
                this.smog.setSpeedY({ min: -50, max: 1 });
                //speedY: { min: -10, max: 10 },
                quantity++;
            }
    
            if(speed_rot<0){
                if(correct==-1){
                    p_sX.min -= 10;
                    p_sX.max -= 20;
                }else{
                    p_sX.min += 10;
                    p_sX.max += 20;
                }
                //this.smog.setSpeedX({ min: 20, max: 50 });
                this.smog.setSpeedY({ min: -1, max: 50 });
                //speedY: { min: -10, max: 10 },
                quantity++;
            }
    
            //if(p_sX.min!=10){
                this.smog.setSpeedX({ min: p_sX.min, max: p_sX.max });
            //}
            this.smog.setQuantity(quantity);
    
            if(speed_rot==0){
                //this.smog.setSpeedX({ min: 10, max: 20 });
                this.smog.setSpeedY({ min: -10, max: 10 });
                //this.smog.setQuantity(1);
            }
        }else{
            if(this.smog){
                this.smog.killAll();
                this.smog.stop();
                this.smog = null;
            }
        }
        
        this.cont.angle = angle;
        
        
        this.tower.update(tower_angle);

        if(this.hp_bar){
            this.hp_bar.update(percent_hp);
        }
        
    }
}