import Tank from '../objects/TankOfline';
import store from "../../store";
import vue from "../../main";
import keyEvent from "../../events/keyboard/index";
import {ballToBall} from "./ballToBall";

export default class PlayerOfline{
    constructor(scene){
        this.scene = scene;
        this.tank = null;
        this.tank2 = null;
        this.is_w = false;
        this.is_s = false;
        this.is_d = false;
        this.is_a = false;
        this.ready_to_del = false;
        this.on_pos = {x:0,y:0};
        this.init();
    }

    init(){
        this.tank = new Tank({
            ...store.state.game.game.player,
            scene:this.scene,
            x:100,
            max_speed:3,
            accel:0.01,
            skid:false
        });

        this.tank2 = new Tank({
            ...store.state.game.game.player,
            scene:this.scene,
            x:550,
            y:200,
            angle:180,
            max_speed:1,
            accel:0.06
        });

        this.tank2.is_s = true;

        // this.tank3 = new Tank({
        //     ...store.state.game.game.player,
        //     scene:this.scene,
        //     x:560,
        //     y:200,
        //     max_speed:4,
        //     accel:0.06
        // });

        // this.tank3.is_w = true;

        // this.tank2 = new Tank({
        //     ...store.state.game.game.player,
        //     scene:this.scene,
        //     max_speed:4
        // });


        keyEvent.add({
            key_code:'KeyW',
            e:'down',
            func:this.downW,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyW',
            e:'up',
            func:this.upW,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyS',
            e:'down',
            func:this.downS,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyS',
            e:'up',
            func:this.upS,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyD',
            e:'down',
            func:this.downD,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyD',
            e:'up',
            func:this.upD,
            context:this            
        });

        keyEvent.add({
            key_code:'KeyA',
            e:'down',
            func:this.downA,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyA',
            e:'up',
            func:this.upA,
            context:this            
        });
    }

    downW(){
        if(!this.is_w){
            //console.log('нажал вперед!');
            this.is_w = true;
            this.tank.is_w = true;
            //this.tank2.is_w = true;
            //this.tank._speed = this.tank._main_speed * -1;
            //this.tank2._speed = this.tank2._main_speed * -1;
        }
    }

    upW(){
        if(this.is_w){
            //console.log('отжал вперед!');
            this.is_w = false;
            this.tank.is_w = false;
            //this.tank._speed = 0;
            //this.tank2.is_w = false;
            //this.tank2._speed = 0;
            //this.tank2._speed = 0;
        }
    }

    downS(){
        if(!this.is_s){
            //console.log('нажад назад!!!');
            this.is_s = true;
            this.tank.is_s = true;
            //this.tank2.is_s = true;
            //this.tank._speed = this.tank._main_speed;
            //this.tank2._speed = this.tank2._main_speed;
        }
    }

    upS(){
        if(this.is_s){
            //console.log('отжал назад!');
            this.is_s = false;
            this.tank.is_s = false;
            //this.tank._speed = 0;
            //this.tank2.is_s = false;
            //this.tank2._speed = 0;
            //this.tank2._speed = 0;
        }
    }

    downD(){
        if(!this.is_d){
            //console.log('нажад в право!!!');
            this.is_d = true;
            this.tank._speed_rot = this.tank._main_speed_rot;
            //this.tank2._speed_rot = this.tank2._main_speed_rot;
        }
    }

    upD(){
        if(this.is_d){
            //console.log('отжал в право!');
            this.is_d = false;
            this.tank._speed_rot = 0;
            //this.tank2._speed_rot = 0;
        }
    }

    downA(){
        if(!this.is_a){
            //console.log('нажад в лево!!!');
            this.is_a = true;
            this.tank._speed_rot = this.tank._main_speed_rot * -1;
            //this.tank2._speed_rot = this.tank2._main_speed_rot * -1;
        }
    }

    upA(){
        if(this.is_a){
            //console.log('отжал в лево!');
            this.is_a = false;
            this.tank._speed_rot = 0;
            //this.tank2._speed_rot = 0;
        }
    }

    update(){
        
        // let correct1_2 = ballToBall(
        //     {
        //         x:this.tank.cont.x,
        //         y:this.tank.cont.y,
        //         angle:this.tank.cont.angle,
        //         speed:this.tank._speed,
        //         forseY:this.tank._force_y,
        //         forseX:this.tank._force_x
        //     },
        //     {
        //         x:this.tank2.cont.x,
        //         y:this.tank2.cont.y,
        //         angle: this.tank2.cont.angle,
        //         speed:this.tank2._speed,
        //         forseY:this.tank2._force_y,
        //         forseX:this.tank2._force_x
        //     }
        //     ,41
        // );

        // let correct2_1 = ballToBall(
        //     {
        //         x:this.tank2.cont.x,
        //         y:this.tank2.cont.y,
        //         angle:this.tank2.cont.angle,
        //         speed:this.tank2._speed,
        //         forseY:this.tank2._force_y,
        //         forseX:this.tank2._force_x
        //     },
        //     {
        //         x:this.tank.cont.x,
        //         y:this.tank.cont.y,
        //         angle: this.tank.cont.angle,
        //         speed:this.tank._speed,
        //         forseY:this.tank._force_y,
        //         forseX:this.tank._force_x
        //     }
        //     ,41
        // );

        // let correct1_3 = ballToBall(
        //     {
        //         x:this.tank.cont.x,
        //         y:this.tank.cont.y,
        //         angle:this.tank.cont.angle,
        //         speed:this.tank._speed,
        //         forseY:this.tank._force_y,
        //         forseX:this.tank._force_x
        //     },
        //     {
        //         x:this.tank3.cont.x,
        //         y:this.tank3.cont.y,
        //         angle: this.tank3.cont.angle,
        //         speed:this.tank3._speed,
        //         forseY:this.tank3._force_y,
        //         forseX:this.tank3._force_x
        //     }
        //     ,41
        // );

        // let correct2_3 = ballToBall(
        //     {
        //         x:this.tank2.cont.x,
        //         y:this.tank2.cont.y,
        //         angle:this.tank2.cont.angle,
        //         speed:this.tank2._speed,
        //         forseY:this.tank2._force_y,
        //         forseX:this.tank2._force_x
        //     },
        //     {
        //         x:this.tank3.cont.x,
        //         y:this.tank3.cont.y,
        //         angle: this.tank3.cont.angle,
        //         speed:this.tank3._speed,
        //         forseY:this.tank3._force_y,
        //         forseX:this.tank3._force_x
        //     }
        //     ,41
        // );

        // if(correct1_2.is_collig){
        //     this.tank.setCorrect(correct1_2.b1);
        //     this.tank2.setCorrect(correct1_2.b2);
        // }

        // if(correct2_1.is_collig){
        //     this.tank.setCorrect(correct2_1.b2);
        //     this.tank2.setCorrect(correct2_1.b1);
        // }

        // if(correct1_3.is_collig){
        //     this.tank.setCorrect(correct1_3.b1);
        //     this.tank3.setCorrect(correct1_3.b2);
        // }

        // if(correct2_3.is_collig){
        //     this.tank2.setCorrect(correct2_3.b1);
        //     this.tank3.setCorrect(correct2_3.b2);
        // }

        this.tank.update();
        this.tank2.update();
        //this.tank3.update();
    }
}