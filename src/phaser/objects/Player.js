import Tank from '../objects/Tank';
import keyEvent from "../../events/keyboard/index";
import vue from '../../main';
import getAngle from "../../services/MathGame";

export default class Player{
    constructor(scene){
        this.scene = scene;
        this.tank = null;
        this.is_w = false;
        this.is_s = false;
        this.is_d = false;
        this.is_a = false;
        this.ready_to_del = false;
        this.on_pos = {x:0,y:0};
        this.time_cd_fire = 3*60;
        this.timer_cd_fire = this.time_cd_fire;
        this.init();
    }

    init(){
        this.tank = new Tank({
            ...vue.$store.state.game.game.player,
            side:'player',
            scene:this.scene
        });

        keyEvent.add({
            key_code:'KeyW',
            e:'down',
            func:this.downW,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyW',
            e:'up',
            func:this.upW,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyS',
            e:'down',
            func:this.downS,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyS',
            e:'up',
            func:this.upS,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyD',
            e:'down',
            func:this.downD,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyD',
            e:'up',
            func:this.upD,
            context:this            
        });

        keyEvent.add({
            key_code:'KeyA',
            e:'down',
            func:this.downA,
            context:this            
        });
        keyEvent.add({
            key_code:'KeyA',
            e:'up',
            func:this.upA,
            context:this            
        });
    }

    downW(){
        if(!this.is_w){
            //console.log('нажад вперед!!!');
            this.is_w = true;
            vue.$socket.emit('key_w',true);
        }
    }

    upW(){
        if(this.is_w){
            //console.log('отжал вперед!');
            this.is_w = false;
            vue.$socket.emit('key_w',false);
        }
    }

    downS(){
        if(!this.is_s){
            //console.log('нажад назад!!!');
            this.is_s = true;
            vue.$socket.emit('key_s',true);
        }
    }

    upS(){
        if(this.is_s){
            //console.log('отжал назад!');
            this.is_s = false;
            vue.$socket.emit('key_s',false);
        }
    }

    downD(){
        if(!this.is_d){
            //console.log('нажад в право!!!');
            this.is_d = true;
            vue.$socket.emit('key_d',true);
        }
    }

    upD(){
        if(this.is_d){
            //console.log('отжал в право!');
            this.is_d = false;
            vue.$socket.emit('key_d',false);
        }
    }

    downA(){
        if(!this.is_a){
            //console.log('нажад в лево!!!');
            this.is_a = true;
            vue.$socket.emit('key_a',true);
        }
    }

    upA(){
        if(this.is_a){
            //console.log('отжал в лево!');
            this.is_a = false;
            vue.$socket.emit('key_a',false);
        }
    }

    del(){
        keyEvent.delAll(['KeyS','KeyW','KeyD','KeyA']);
        this.ready_to_del = true;
        vue.$store.commit('game/delGame');
    }

    forceBattle(){
        this.tank.forceBattle();
    }

    update(){
        //if(vue.$store.state.game.game){
            // if(store.state.game.game.status==='game_over'){
            //     keyEvent.delAll(['KeyS','KeyW','KeyD','KeyA']);
            //     this.scene.game.canvas.removeEventListener('mousemove', this.onMouseMove);
            //     this.ready_to_del = true;
            //     //store.commit('game/delGame');
            //     return;
            // }

            const {cd_fire}=vue.$store.state.game.game.player;
            //console.log('tower_angle = ', tower_angle);
            // if(this.tank.cont.x!=x||this.tank.cont.y!=y||this.tank.cont.angle!=angle){
            //     vue.$socket.emit('mouse_move',getAngle(
            //         {
            //             tank:vue.$store.getters['game/getTankPos'],
            //             mouse:this.scene.pointer,
            //             angle:vue.$store.getters['game/getTowerAngle']
            //         }
            //     ));
            // }

            this.tank.update(vue.$store.state.game.game.player);
            
            //console.log('cd_fire = ',cd_fire);
            if(cd_fire){
                // if(this.timer_cd_fire>=this.time_cd_fire){
    
                // }
                this.timer_cd_fire--;
                vue.$store.commit('game/updateTimerCdGun', this.timer_cd_fire);
            }else{
                this.timer_cd_fire = this.time_cd_fire;
                vue.$store.commit('game/updateTimerCdGun', 0);
            }
            //this.tank.tower.update(angle,x,y,{x:this.scene.pointer.worldX,y:this.scene.pointer.worldY});
       // }
    }
}