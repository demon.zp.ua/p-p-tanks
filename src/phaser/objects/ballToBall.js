export const ballToBall=(b1,b2,summ_r)=>{
    let r1 = Math.sqrt((b1.x - b2.x)*(b1.x - b2.x) + 
                (b1.y - b2.y)*(b1.y - b2.y));
    let xmovb2 = 0;
    let ymovb2 = 0; 
    let xmovb1 = 0;
    let ymovb1 = 0;
    let is_collig = false;
    if(r1<=summ_r){
        let mDxb2 = b1.x - b2.x;
		let mDyb2 = b1.y - b2.y;
        let tanb2 = Math.atan2(mDyb2, mDxb2);
        let mAngleb2 = tanb2 / Math.PI * 180;
        //this.cont.angle * Math.PI / 180;

        let mDxb1 = b2.x - b1.x;
        let mDyb1 = b2.y - b1.y;
        
        console.log(mDxb1,'|',mDyb1,'||',mDxb2,'|',mDyb2);
        let tanb1 = Math.atan2(mDyb1, mDxb1);
        let mAngleb1 = tanb1 / Math.PI * 180;
        let forseb1X = 1;
        let forseb1Y = 1;
        let forseb2X = 1;
        let forseb2Y = 1;
        if(b1.forseY==0.4){
            forseb2X = 0;
            //console.log('forseX = 0');
        }

        if(b2.forseY==0.4){
            forseb1X = 0;
        }

        if(b1.forseY==0.4){
            forseb2X = 0;
        }

        if(b1.forseY==0.4){
            forseb2X = 0;
        }

        // if(b1.forseX==0.4 || b2.forseX==0.4){
        //     forseY = 0;
        // }
        xmovb2 = (-Math.abs(b1.speed)*0.5) * Math.cos(tanb2);
        ymovb2 = (-Math.abs(b1.speed)*0.5) * Math.sin(tanb2);

        //xmovb2 = (b1.speed*0.5) * Math.cos(b1.angle * Math.PI / 180);
        //ymovb2 = (b1.speed*0.5) * Math.sin(b1.angle * Math.PI / 180);
        //let speedb2 = (xmovb2/0.5)/Math.cos(tanb2);
        //xmovb1 = (- Math.abs(b2.speed)) * Math.cos(tanb1);
        //ymovb1 = (- Math.abs(b2.speed)) * Math.sin(tanb1);
        xmovb1 = (-Math.abs(b1.speed)*0.5 - Math.abs(b2.speed)) * Math.cos(tanb1);
        ymovb1 = (-Math.abs(b1.speed)*0.5 - Math.abs(b2.speed)) * Math.sin(tanb1);
        //xmovb1 = (b1.speed*0.5 -Math.abs(b2.speed)) * Math.cos(b1.angle * Math.PI / 180);
        //ymovb1 = (b1.speed*0.5 -Math.abs(b2.speed)) * Math.sin(b1.angle * Math.PI / 180);
        //let coll_angle_b1 = b2.angle;
        // let ang1 = 360 - (b1.angle - b2.angle);
        // let f_angle_b2 = 180 - ang1;
        // let f_angle_b1 = b2.angle;
        //console.log('столкнулись!!!! = ', speedb2, "||",b1.speed);
        //console.log('столкнулись!!!! = ', mAngleb2,'||',mAngleb1);
        is_collig = true;
    }
    //xmovb2 = (-Math.abs(b1.speed)*0.5) * Math.cos(174 * Math.PI / 180);
    //ymovb2 = (-Math.abs(b1.speed)*0.5) * Math.sin(174 * Math.PI / 180);

    return {
        b1:{
            xmov:xmovb1,
            ymov:ymovb1
        },
        b2:{
            xmov:xmovb2,
            ymov:ymovb2
        },
        is_collig
    };
}

// import flash.events.Event;

// stop();

// var game={};
// game.numBalls=3;
// var stolknovenie = false;
// var whatTime=0;

// for (var i = 1; i <= game.numBalls; i ++) {
// 	var ballname="ball"+i;
// 	game[ballname]={};
// 	game[ballname].clip=root[ballname];
// 	game[ballname].xpos=game[ballname].clip.x;
// 	game[ballname].ypos=game[ballname].clip.y;
// 	game[ballname].radius=game[ballname].clip.width/2;
// 	game[ballname].xmov=0;
// 	game[ballname].ymov=0;
// 	if(i==2){
// 		game[ballname].mass = 2;
// 	}else{
// 		game[ballname].mass = 1;
// 	}
// }

// game.ball1.xmov=5;
// game.ball1.ymov=0;
// game.ball2.xmov=-2;
// game.ball2.ymov=-1;
// game.ball3.ymov=-5;

// function moveBalls() {
//     for (var i=1; i<=game.numBalls; i++) {
//         var obj = game["ball"+i] as Object;
//         obj.tempx = obj.xpos + obj.xmov;
//         obj.tempy = obj.ypos + obj.ymov;
// 	}
// }

// function renderBalls() {
// 	for (var i = 1; i <= game.numBalls; i ++) {
// 		var obj=game["ball"+i] as Object;
// 		obj.xpos=obj.tempx;
// 		obj.ypos=obj.tempy;
// 		obj.clip.x=obj.xpos;
// 		obj.clip.y=obj.ypos;
// 	}
// }

// function ballToBallDetect(b1, b2) {
// 	stolknovenie = false;
// 	var xmov1=b1.xmov;
// 	var ymov1=b1.ymov;
// 	var xmov2=b2.xmov;
// 	var ymov2=b2.ymov;

// 	var xl1=b1.xpos;
// 	var yl1=b1.ypos;
// 	var xl2=b2.xpos;
// 	var yl2=b2.ypos;
	
// 	var R = b1.radius + b2.radius;
// 	var a = -2 * xmov1 * xmov2 + xmov1 * xmov1 + xmov2 * xmov2;
// 	var b = -2 * xl1 * xmov2 - 2 * xl2 *xmov1 + 2 * xl1 *xmov1 + 2 * xl2 * xmov2;
// 	var c = -2 * xl1 * xl2 + xl1 * xl1 + xl2 * xl2;
// 	var d = -2 * ymov1 * ymov2 + ymov1 * ymov1 + ymov2 * ymov2;
// 	var e = -2 * yl1 * ymov2 - 2 * yl2 *ymov1 + 2 * yl1 *ymov1 + 2 * yl2 * ymov2;
// 	var f = -2 * yl1 * yl2 + yl1 * yl1 + yl2 * yl2;
// 	var g = a + d;
// 	var h = b + e;
// 	var k = c + f - R * R;
	
// 	var koren = Math.sqrt(h*h-4*g*k);
// 	var t1 = (-h + koren) / (2 * g);
// 	var t2 = (-h - koren) / (2 * g);

// 	if (t1 > 0 && t1 <= 1) {
// 		whatTime = t1;
// 		var stolknovenie = true;
// 	}

// 	if (t2 > 0 && t2 <= 1) {
// 		if (whatTime == 0 || t2 < t1) {
// 			whatTime = t2;
// 			stolknovenie = true;
// 		}
// 	}
// 	if (stolknovenie) {
// 		trace("Есть столкновение!t1 =", t1, "t2 =", t2);
// 		ball2BallReaction(b1 , b2, xl1, xl2, yl1, yl2, whatTime);
// 	}
// }

// function ball2BallReaction(b1, b2, x1, x2, y1, y2, time){
// 	var mass1 = b1.mass;
// 	var mass2 = b2.mass;
	
// 	var xVel1 = b1.xmov;
// 	var xVel2 = b2.xmov;
// 	var yVel1 = b1.ymov;
// 	var yVel2 = b2.ymov;
// 	var run = (x1 - x2);
// 	var rise = (y1 - y2);
	
// 	var Alfa = Math.atan2(rise, run);
// 	var cosAlfa = Math.cos(Alfa);
// 	var sinAlfa = Math.sin(Alfa);
	
// 	var xVel1prime = xVel1 * cosAlfa + yVel1 * sinAlfa;
// 	var xVel2prime = xVel2 * cosAlfa + yVel2 * sinAlfa;
	
// 	var yVel1prime = yVel1 * cosAlfa - xVel1 * sinAlfa;
// 	var yVel2prime = yVel2 * cosAlfa - xVel2 * sinAlfa;
	
// 	var P = (mass1 * xVel1prime + mass2 * xVel2prime);
// 	var V = (xVel1prime - xVel2prime);
// 	var v2f = (P + mass1 * V)/(mass1 + mass2);
// 	var v1f = v2f - xVel1prime + xVel2prime;
// 	xVel1prime = v1f;
// 	xVel2prime = v2f;
	
// 	xVel1 = xVel1prime * cosAlfa - yVel1prime * sinAlfa;
// 	xVel2 = xVel2prime * cosAlfa - yVel2prime * sinAlfa;
// 	yVel1 = yVel1prime * cosAlfa + xVel1prime * sinAlfa;
// 	yVel1 = yVel1prime * cosAlfa + xVel1prime * sinAlfa;
	
// 	b1.tempx = b1.xpos + b1.xmov * time * 0.09;
// 	b1.tempy = b1.ypos + b1.ymov * time * 0.09;
// 	b2.tempx = b2.xpos + b2.xmov * time * 0.09;
// 	b2.tempy = b2.ypos + b2.ymov * time * 0.09;
	
// 	b1.xmov = xVel1;
// 	b2.xmov = xVel2;
// 	b1.ymov = yVel1;
// 	b2.ymov = yVel2;
// }

// stage.addEventListener(Event.ENTER_FRAME, ef);
// function ef(e:Event) {
// 	moveBalls();
// 	ballToBallDetect(game.ball1, game.ball2);
// 	ballToBallDetect(game.ball1, game.ball3);
// 	ballToBallDetect(game.ball2, game.ball3);
// 	renderBalls();
// }