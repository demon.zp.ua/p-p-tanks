export default class Bullet{
    constructor({scene,x,y,id}){
        this.scene = scene;
        this.x = x;
        this.y = y;
        this.id = id;
        //this.cont = null;
        this.w = 10;
        this.h = 10;
        this._sprite = null;
        this.render();
    }

    render(){
        //this.cont = this.scene.add.container(0, 0);
        this._sprite = this.scene.add.sprite(this.x, this.y, 'bullet1')
        .setDepth(1)
        .setDisplaySize(this.w,this.h);

        //this.cont.add(this._sprite);
    }

    del(){
        //console.log('должен удалить со сцены!!!');
        this._sprite.destroy();
    }

    update({x,y}){
        //console.log('x = ',x);
        this._sprite.x = x;
        this._sprite.y = y;
    }
}