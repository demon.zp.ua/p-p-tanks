export default class WallAngleOfl{
    constructor({x,y,angle,scene}){
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.scene = scene;
        this.sprite = null;

        this.width_half = 40;
        this.height_half = 40;
        this.x0 = -20;
        this.x1 = 20;
        this.y0 = -20;
        this.y1 = 20;

        let t_angle = angle * Math.PI / 180;

        let kY = Math.sin(t_angle);
        let kX = Math.cos(t_angle);

        this.real_x0 = this.x - 20*kX + 20 * kY;
        this.real_x1 = this.x + 20*kX - 20 * kY;
        this.real_y0 = this.y - 20*kY - 20 * kX;
        this.real_y1 = this.y + 20*kY + 20 * kX;

        this.real_cor_x0 = this.x + (this.x0+3)*kX;
        this.real_cor_x1 = this.x + (this.x1-3)*kX;
        this.real_cor_y0 = this.y + (this.y0+3)*kY;
        this.real_cor_y1 = this.y + (this.y1-3)*kY;


        this.dotx1 = {
            x:this.x + this.x0*kX - this.y0 * kY,
            y:this.y + this.y0*kX + this.y0 * kY
        };
        this.dotx2 = {
            x:this.x + 20*kX - 20 * kY,
            y:this.y - 20*kX + 20 * kY
        };

        this.dotx3 = {x:this.real_x1,y:this.real_cor_y0};
        this.dotx4 = {x:this.real_x1,y:this.real_cor_y1};

        this.dotx3_1 = {x:this.real_cor_x1,y:this.real_cor_y0};
        this.dotx4_1 = {x:this.real_cor_x1,y:this.real_cor_y1};

        this.doty1 = {x:this.real_cor_x0,y:this.real_y0};
        this.doty2 = {x:this.real_cor_x0,y:this.real_y1};

        this.doty3 = {x:this.real_cor_x1,y:this.real_y0};
        this.doty4 = {x:this.real_cor_x1,y:this.real_y0};

        this.doty2_1 = {x:this.real_cor_x0,y:this.real_cor_y1};
        this.doty4_1 = {x:this.real_cor_x1,y:this.real_cor_y1};

        

        this.render();
    }

    render(){
        let dot1 = this.scene.add.circle(this.dotx1.x, this.dotx1.y, 6, 0xff66ff);
        
        this.sprite = this.scene.add.sprite(200, 200, 'wall')
        .setDisplaySize(40,40)
        .setAngle(this.angle);
        let dot2 = this.scene.add.circle(this.dotx2.x, this.dotx2.y, 6, 0x6633ff);
        
        //let dot3 = this.scene.add.circle(this.real_x1, this.real_y0, 6, 0x6600ff);
        //let dot4 = this.scene.add.circle(this.real_x1, this.real_y1, 6, 0xff00ff);
    }

    colligionX(x,y){
        //console.log('пытаюсь просчитать x столкновен!',this.x0,'||',this.x1);
        //let t_x = x;
        let Dx = this.dotx2.x - this.dotx1.x;
		let Dy = this.dotx2.y - this.dotx1.y;
        let d1 = ((this.dotx1.y - y) * Dx + (x - this.dotx1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.dotx3.x - this.dotx1.x;
		Dy = this.dotx3.y - this.dotx1.y;
						
		let d2 = ((this.dotx1.y - y) * Dx + (x - this.dotx1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.dotx2.x - this.dotx4.x;
		Dy = this.dotx2.y - this.dotx4.y;
						
        let d3 = ((this.dotx4.y - y) * Dx + (x - this.dotx4.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.dotx3.x - this.dotx4.x;
		Dy = this.dotx3.y - this.dotx4.y;
						
        let d4 = ((this.dotx4.y - y) * Dx + (x - this.dotx4.x) * Dy ) / (Dy * Dy + Dx * Dx);
        
        Dx = this.dotx3_1.x - this.dotx4_1.x;
		Dy = this.dotx3_1.y - this.dotx4_1.y;
						
		let d4_1 = ((this.dotx4_1.y - y) * Dx + (x - this.dotx4_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        if(d1>0 && d2<0 && d3<0 && d4_1>0){
            return this.x0;
        }

        if(d4>0 && d2<0 && d3<0 && d1>0){
            return this.x1;
        }

        return x;
    }

    colligionY(x,y){
        let Dx = this.doty3.x - this.doty1.x;
		let Dy = this.doty3.y - this.doty1.y;
        let d2 = ((this.doty1.y - y) * Dx + (x - this.doty1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        // Dx = this.dot2_1.x - this.dot1_1.x;
        // Dy = this.dot2_1.y - this.dot1_1.y;
        
        // let d1_1 = ((this.dot1_1.y - y) * Dx + (x - this.dot1_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.doty2.x - this.doty1.x;
		Dy = this.doty2.y - this.doty1.y;
						
		let d1 = ((this.doty1.y - y) * Dx + (x - this.doty1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.doty3.x - this.doty4.x;
		Dy = this.doty3.y - this.doty4.y;
						
        let d3 = ((this.doty4.y - y) * Dx + (x - this.doty4.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.doty2.x - this.doty4.x;
		Dy = this.doty2.y - this.doty4.y;
						
        let d4 = ((this.doty4.y - y) * Dx + (x - this.doty4.x) * Dy ) / (Dy * Dy + Dx * Dx);
        
        Dx = this.doty2_1.x - this.doty4_1.x;
		Dy = this.doty2_1.y - this.doty4_1.y;
						
		let d4_1 = ((this.doty4_1.y - y) * Dx + (x - this.doty4_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        if(d2<0 && d1>0 && d3>0 && d4_1<0){
            return this.y0;
        }
        if(d4<0 && d1>0 && d3>0 && d2<0){
            return this.y1;
        }

        // if(d4>0 && d1<0 && d3<0 && d2>0){
        //     return this.y1;
        // }

        return y;
    }
}