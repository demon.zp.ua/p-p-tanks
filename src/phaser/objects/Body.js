export default class Body{
    constructor(scene){
        this.w = 40;
        this.h = 40;
        this.scene = scene;
        this.sprite = null;
        this.render();
    }

    render(){
        this.sprite = this.scene.add.sprite(0, 0, 'body')
        .setDisplaySize(this.w,this.h);
    }
}