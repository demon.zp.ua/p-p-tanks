import store from '../../store';
import Bullet from './Bullet';

export default class Bullets{
    constructor(scene){
        this.scene = scene;
        //this.arr_map = [];
        this.bullets = {};
        //this.init();
    }

    addBullets(){
        // console.log('должен нарисовать Пулю!!!');
        // store.state.game.add_bullets.forEach((b) => {
        //     this.bullets[b.id] = new Bullet({
        //         ...b,
        //         scene:this.scene
        //     });
        // });
        // store.commit('game/setAddBullets',[]);
    }

    dellBullet(id){
        this.bullets[id].del();
        delete this.bullets[id];
    }

    dellBullets(){
        store.state.game.del_bullets.forEach((b) => {
            this.bullets[b.id].del();
            delete this.bullets[b.id];
        });
        store.commit('game/setDelBullets',[]);
    }

    async toDelBullet(){
        for (const key in this.bullets) {
            if(!store.state.game.game.bullets.find((b)=>b.id===key)){
                this.dellBullet(key);
            }
            // if (object.hasOwnProperty(key)) {
            //     const element = object[key];
                
            // }
        }
        // for (const key of this.bullets) {
        //     if(!store.state.game.game.bullets.find((b)=>b.id===key)){
        //         dellBullet(key);
        //     }
        // }
    }

    update(){
        if(store.state.game.game){
            // if(store.state.game.add_bullets.length>0){
            //     this.addBullets();
            // }

            if(store.state.game.del_bullets.length>0){
                this.dellBullets();
            }

            this.toDelBullet();

            store.state.game.game.bullets.forEach((b)=>{
                if(this.bullets.hasOwnProperty(b.id)){
                    this.bullets[b.id].update({
                        x:b.x,
                        y:b.y
                    });
                }else{
                    this.bullets[b.id] = new Bullet({
                        ...b,
                        scene:this.scene
                    });
                }
            });

            
        }
    }

}