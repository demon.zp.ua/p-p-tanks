import vue from "../../main";

export default class TimeOute{
  constructor(scene){
    this.scene = scene;
    this.delay = 5;
    this._timeText = this.delay;
    this.textLabel = this.delay;
    this._time = 0;
    this.text = null;
    this.create();
    this.timeOut = null;
  }

  create(){
    this.text = this.scene.add.text(800/2, 600/2, this.textLabel, { fontFamily: 'Arial', fontSize: 64, color: '#df0a00' });
  }

  update(delta){
    this._time = this._time+1*delta;
    const serverTime = Math.trunc(vue.$store.state.game.game.time/1000);
    //vue.$store.state.game.game.time

    console.log('time = ', serverTime);
    this.text.scale += 0.02;
    this.text.alpha -= 0.01;
    if(this._time>=1000){
      this._time = 0;
      this.onRepeatHandle(serverTime);
    }
    if(serverTime>=this.delay && this.textLabel!=='Fight!'){
      this._timeText = 1;
      this.onRepeatHandle(serverTime);
    }
  }

  del(){
    this.text.destroy();
    this.scene.delTimeOut();
  }

  // setTimer(){
  //   this.timeOut = setTimeout(this.onRepeatHandle.bind(this), 1000);
  // }

  onRepeatHandle(serverTime){
    //console.log('повторяю');
    if(this.textLabel==='Fight!'){
      //console.log('onRepeatHandle');
      this.del();
      return;
    }
    this.text.scale = 1;
    this.text.alpha = 1;
    this._timeText = this.delay-serverTime;
    this.textLabel = this._timeText;

    if(this._timeText<=0){
      this.textLabel = 'Fight!';
    }
    
    this.text.setText(this.textLabel);
  }
}