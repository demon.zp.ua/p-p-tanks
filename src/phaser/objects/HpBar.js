export default class HpBar{
    constructor(scene,side){
        this.scene = scene;
        
        this.side = side;
        this.t_angle = 0;
        this.w = 30;
        this.h = 6;
        this.hp = null;
        this.fon = null;
        this.cont = null;
        this.render();
    }

    render(){
        this.cont = this.scene.add.container(0, 28);
        this.hp = this.scene.add.rectangle(0, 0, this.w, this.h, 0xff0000);
        this.fon = this.scene.add.rectangle(0, 0, this.w, this.h, 0x8f929c);

        this.cont.add([this.fon,this.hp]);
        this.cont.angle = 180;
    }

    update(percent){
        let w = this.w*percent/100;
        if(percent<0){
            w = 0;
        }
        
        this.hp.width = w;
    }
};