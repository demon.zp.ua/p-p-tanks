import Body from './Body';
import Tower from './Tower';
import store from '../../store';

export default class TankOfline{
    constructor({scene,id,x,y,angle,max_speed,accel,skid}){
        this.scene = scene;
        this.id = id;
        this.cont = null;
        this.body = null;
        this.tower = null;
        this.x = x;
        this.y = y;
        this.rot = angle;


        this.is_w = false;
        this.is_s = false;
        if(skid===false){
            this.skid = skid;
        }else{
            this.skid = true;
        }
        this._acceleration = accel;
        this._slowdown = 0.98;
        this._main_speed = max_speed;
        this._main_speed_rot = 2;
        this._speed_rot = 0;

        this._t_angle_rot = 0;
        this._speed = 0;
        this._forse_skid = 0;
        this._forse_x = 1;
        this._force_y = 1;
        this.num_walls = this.scene.walls.length;
        this.num_colligion = 0;
        this.colligion_mov = {
            xmov:0,
            ymov:0
        }
        console.log(this.skid);
        this.render();
    }

    render(){
        this.cont = this.scene.add.container(this.x, this.y);

        this.body = new Body(this.scene);
        this.tower = new Tower(this.scene);

        this.cont.add([this.body.sprite, this.tower.cont]);
        this.cont.angle = this.rot;
        console.log('танк = ',this);
        //this.scene.matter.add.gameObject(this.cont);
    }

    slowdown(){
        if (Math.abs(this._speed)>0.5) {
			this._speed *= this._slowdown;
		// если скорость мала, то делаем 0
		} else {
			this._speed = 0;
		}
    }

    acceleration(){
        if(this.is_w){
            this._speed =-this._main_speed;
            // this._speed-=this._acceleration;
            // if(this._speed>0){
            //     this._speed -= 0.1;
            // }
            // if(this._speed<=-this._main_speed){
            //     this._speed =-this._main_speed;
            // }
        }else if(this.is_s){
            this._speed =this._main_speed;
            // this._speed+=this._acceleration;
            // if(this._speed<0){
            //     this._speed += 0.1;
            // }
            // if(this._speed>=this._main_speed){
            //     this._speed = this._main_speed;
            // }
        }else{
            this._speed = 0;
            //this.slowdown();
        }
        //console.log(this._speed);
    }

    setCorrect(colligion_mov){
        this.colligion_mov.xmov += colligion_mov.xmov;
        this.colligion_mov.ymov += colligion_mov.ymov;
    }

    update(){

        this.acceleration();
        
        if(this._speed_rot!=0){
            if(this._speed!=0){
                if(Math.abs(this._speed)>=4){
                    this.cont.angle += this._speed_rot/0.5;
                    this._t_angle_rot += this._speed_rot/0.5;
                }else if(Math.abs(this._speed)>=3){
                    this.cont.angle += this._speed_rot/0.66;
                    this._t_angle_rot += this._speed_rot/0.66;
                }else if(Math.abs(this._speed)>=2){
                    this.cont.angle += this._speed_rot/0.8;
                    this._t_angle_rot += this._speed_rot/0.8;
                }else{
                    this.cont.angle += this._speed_rot;
                    this._t_angle_rot += this._speed_rot;
                }
                
                if(Math.abs(this._t_angle_rot)>=40){
                    if(Math.abs(this._speed)>=2.8){
                        this._forse_skid = -(Math.abs(this._speed)*0.4);
                    }
                }else{
                    this._forse_skid = 0;
                }
                // if(this.skid){
                //     this._forse_skid = 1.9;
                // }
                //console.log('увеличеная скорость вращения');
            }else{
                this.cont.angle += this._speed_rot;
                this._t_angle_rot = 0;
                //console.log('уменшеная');
            }
            
        }else{
            this._t_angle_rot = 0;
        }

        let angle = this.cont.angle * Math.PI / 180;
        //console.log(this._t_angle_rot);
        let f_angle = 0;
        if(this._speed_rot>0 ){
            if(this._speed<0){
                f_angle = (this.cont.angle-100) *Math.PI/180;
            }else{
                f_angle = (this.cont.angle+100) *Math.PI/180;
            }
            
        }else if(this._speed_rot<0){
            if(this._speed<0){
                f_angle = (this.cont.angle+100) *Math.PI/180;
            }else{
                f_angle = (this.cont.angle-100) *Math.PI/180;
            }
            //f_angle = (this.cont.angle+90) *Math.PI/180;
        }
        
        // if(this._speed_rot>0){
        //     //let f_angle = this.cont.angle + 90 *Math.PI/180;

        //     let kY = Math.sin(angle);
        //     let kX = Math.cos(angle);

        //     let x = this.cont.x + 0*kX + 20*kY;
        //     let y = this.cont.y + 0*kY + 20*kX;

        //     let mDx2 = this.cont.x - x;
		// 	let mDy2 = this.cont.y - y;
		// 	let mAngle = Math.atan2(mDy2, mDx2);
		// 	//получаем угол между мышкой и башней в градусах
        //     let mAngle2 = mAngle / Math.PI * 180;
        //     console.log("mAngle2 = ", mAngle2);
        //     console.log("+90 = ",(this.cont.angle + 90)*-1);
        //     let dot1 = this.scene.add.circle(x, y, 6, 0xff66ff);
           
        // }
        //let angle2 = this.cont.angle + 90 * Math.PI / 180;
        //console.log(this.cont.angle);
        //console.log((this.cont.angle + 90)*-1);
        //this.cont.x+=colligion_mov.b1.xmov;
        //this.cont.y+=colligion_mov.b1.ymov;
        //if(this._speed!=0){
            // if(colligion_mov.xmov!=1 || colligion_mov.ymov!=1){
            //     this._speed = 1;
            // }
            //let forse_skid_x =  this._forse_skid * Math.cos(f_angle);
            //let forse_skid_y =  this._forse_skid * Math.sin(f_angle);
            //console.log(this._forse_skid);
		    let xmov = this._speed * this._forse_x * Math.cos(angle);
            let ymov = this._speed * this._force_y * Math.sin(angle);
            // let b = Math.atan(ymov/xmov);
            // let b_forse = this._forse_skid * b;
            // xmov = this._speed * b_forse * Math.cos(angle);
            // ymov = this._speed * b_forse * Math.sin(angle);
            this.cont.x+=xmov + this.colligion_mov.xmov;
            this.cont.y+=ymov + this.colligion_mov.ymov;
            this.colligion_mov.xmov = 0;
            this.colligion_mov.ymov = 0;
            //this.cont.x+=colligion_mov.xmov;
            //this.cont.y+=colligion_mov.ymov;
            // if(this._speed_rot!=0){
            //     this.cont.x+=forse_skid_x;
            //     this.cont.y+=forse_skid_y;
            // }
            //console.log(colligion_mov);
            

            let corrector = null;
            // this.scene.walls.forEach(wall => {
            //     this.cont.x = wall.colligionX(this.cont.x,this.cont.y).x;
            //     this.cont.y = wall.colligionY(this.cont.x,this.cont.y).y;
            // });
            //let from_date = Date.now();
            this.scene.walls.forEach(wall => {
                let result = wall.colligion(this.cont.x,this.cont.y);
                if(result.forseX!=1 || result.forseY!=1){
                    corrector = result;
                    this.cont.x = corrector.x;
                    this.cont.y = corrector.y;
                    //this.num_colligion++;
                }
                //this._force_y = wall.colligionY(this.cont.x,this.cont.y);
            });
            //let to_date = Date.now();

            //let delta = from_date - to_date;

            //console.log("delta = ", delta);

            if(corrector){
                // this.cont.x = corrector.x;
                // this.cont.y = corrector.y;
                this._force_y = corrector.forseY;
                this._forse_x = corrector.forseX;
            }else{
                this._force_y = 1;
                this._forse_x = 1;
            }
        //}

        
        
        //this.tower.update();
    }
}