export default class WallOfline{
    constructor({x,y,scene,i}){
        this.x = x;
        this.y = y;
        this.scene = scene;
        this.sprite = null;

        this.width_half = 40;
        this.height_half = 40;
        this.x0 = this.x-this.width_half;
        this.x1 = this.x+this.width_half;
        this.y0 = this.y-this.height_half;
        this.y1 = this.y+this.height_half;

        this.dotx1 = {x:this.x0,y:this.y0+10};
        this.dotx2 = {x:this.x0,y:this.y1-10};

        this.dotx3 = {x:this.x1,y:this.y0+10};
        this.dotx4 = {x:this.x1,y:this.y1-10};

        this.dotx3_1 = {x:this.x1-10,y:this.y0+10};
        this.dotx4_1 = {x:this.x1-10,y:this.y1-10};

        this.doty1 = {x:this.x0+10,y:this.y0};
        this.doty2 = {x:this.x0+10,y:this.y1};

        this.doty3 = {x:this.x1-10,y:this.y0};
        this.doty4 = {x:this.x1-10,y:this.y1};

        this.doty2_1 = {x:this.x0+10,y:this.y1-10};
        this.doty4_1 = {x:this.x1-10,y:this.y1-10};

        if(i<=9){
            this.render();
        }
        
    }

    render(){
        this.sprite = this.scene.add.sprite(this.x, this.y, 'wall')
        .setDisplaySize(40,40);
    }

    colligion(x,y){
        let colligionX = this.colligionX(x,y);
        let colligionY = this.colligionY(x,y);
        return {...colligionX,...colligionY};
    }

    colligionX(x,y){

        let Dx = this.dotx2.x - this.dotx1.x;
		let Dy = this.dotx2.y - this.dotx1.y;
        let d1 = ((this.dotx1.y - y) * Dx + (x - this.dotx1.x) * Dy ) / (Dy * Dy + Dx * Dx);

		if(d1<0){
            //console.log('xd1<0');
            return {x,forseY:1};
        }

        Dx = this.dotx3.x - this.dotx1.x;
        Dy = this.dotx3.y - this.dotx1.y;

		let d2 = ((this.dotx1.y - y) * Dx + (x - this.dotx1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        if(d2>0){
            return {x,forseY:1};
        }

        Dx = this.dotx2.x - this.dotx4.x;
		Dy = this.dotx2.y - this.dotx4.y;
						
        let d3 = ((this.dotx4.y - y) * Dx + (x - this.dotx4.x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d3>0){
            return {x,forseY:1};
        }

        Dx = this.dotx3_1.x - this.dotx4_1.x;
		Dy = this.dotx3_1.y - this.dotx4_1.y;
						
		let d4_1 = ((this.dotx4_1.y - y) * Dx + (x - this.dotx4_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d1>0 && d2<0 && d3<0 && d4_1>0){
        //     return {x:this.x0,forseY:0.4};
        // }

        if(d4_1>0){
            return {x:this.x0,forseY:0.4};
        }

        Dx = this.dotx3.x - this.dotx4.x;
		Dy = this.dotx3.y - this.dotx4.y;
						
        let d4 = ((this.dotx4.y - y) * Dx + (x - this.dotx4.x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d4>0 && d2<0 && d3<0 && d1>0){
        //     return {x:this.x1,forseY:0.4};
        // }
        if(d4>0){
            return {x:this.x1,forseY:0.4};
        }

        return {x,forseY:1};
    }

    colligionY(x,y){
        let Dx = this.doty3.x - this.doty1.x;
		let Dy = this.doty3.y - this.doty1.y;
        let d2 = ((this.doty1.y - y) * Dx + (x - this.doty1.x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d2>0){
            //console.log('yd2>0');
            return {y,forseX:1};
        }
        // Dx = this.dot2_1.x - this.dot1_1.x;
        // Dy = this.dot2_1.y - this.dot1_1.y;
        
        // let d1_1 = ((this.dot1_1.y - y) * Dx + (x - this.dot1_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this.doty2.x - this.doty1.x;
		Dy = this.doty2.y - this.doty1.y;
						
		let d1 = ((this.doty1.y - y) * Dx + (x - this.doty1.x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d1<0){
            return {y,forseX:1};
        }
        Dx = this.doty3.x - this.doty4.x;
		Dy = this.doty3.y - this.doty4.y;
						
        let d3 = ((this.doty4.y - y) * Dx + (x - this.doty4.x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d3<0){
            return {y,forseX:1};
        }
        
        Dx = this.doty2_1.x - this.doty4_1.x;
		Dy = this.doty2_1.y - this.doty4_1.y;
						
		let d4_1 = ((this.doty4_1.y - y) * Dx + (x - this.doty4_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d2<0 && d1>0 && d3>0 && d4_1<0){
        //     return {y:this.y0,forseX:0.4};
        // }
        if(d4_1<0){
            return {y:this.y0,forseX:0.4};
        }

        Dx = this.doty2.x - this.doty4.x;
		Dy = this.doty2.y - this.doty4.y;
						
        let d4 = ((this.doty4.y - y) * Dx + (x - this.doty4.x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d4<0 && d1>0 && d3>0 && d2<0){
        //     return {y:this.y1,forseX:0.4};
        // }
        if(d4<0){
            return {y:this.y1,forseX:0.4};
        }

        return {y,forseX:1};
    }
}