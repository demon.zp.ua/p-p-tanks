export default class Wall{
    constructor({x,y,scene}){
        this.x = x;
        this.y = y;
        this.scene = scene;
        this.sprite = null;
        this.render();
    }

    render(){
        this.sprite = this.scene.add.sprite(this.x, this.y, 'wall')
        .setDisplaySize(40,40);
    }
}