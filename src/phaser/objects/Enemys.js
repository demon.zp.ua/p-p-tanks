import Tank from '../objects/Tank';
import store from "../../store";

export default class Enemys{
    constructor(scene){
        this.scene = scene;
        this.tanks = {};
        this.init();
    }

    init(){
        store.state.game.game.enemys.forEach((enemy) => {
            this.tanks[enemy.id] = new Tank({
                ...enemy,
                side:'enemy',
                scene:this.scene
            });
        });
    }

    forceBattle(){
        for (const enemyId in this.tanks) {
            this.tanks[enemyId].forceBattle();
        }
    }

    update(){
        if(store.state.game.game){
            store.state.game.game.enemys.forEach(enemy => {
                //const {x,y,angle,speed_rot,speed,percent_hp,tower_angle} = enemy;
                //console.log('percent_hp = ', percent_hp);
                this.tanks[enemy.id].update(enemy);
            });
        }
    }
}