export default {
    namespaced: true,
    state: {
        is_loading: false,
        googleAuth: null,
        user:{
            id:null,
            name:null,
            game:null,
            room:null,
            socket_id:null
        },
        is_connect:false
    },
    mutations: {
        setIsLoading(state, payload){
            state.is_loading = payload;
        },
        setGoogleAuth(state, googleAuth){
            state.googleAuth = googleAuth;
        },
        setUser(state, user){
            //console.log("принемаю юзера = ", user);
            // for(let prop in state.user){
            //     if(prop in user){
            //         state.user[prop] = user[prop];
            //     }
            // }

            state.user = user;
            if(user){
                state.is_loading = false;
            }
            //console.log("создал юзера = ", state.user);
        },
        delUser(state){
            //console.log('удалили узера');
            state.user= {
                id:null,
                name:null,
                game:null,
                room:null,
                socket_id:null
            }
            this.commit('rooms/setRooms', null);
            this.commit('rooms/updateRoom', null);
            this.commit('game/setGame', null);
        }
    },
    actions: {
        
    },
    getters: {
        getAuth:state=>{
            if(state.user.id){
                return true;
            }
            //console.log("не авторизирован!!");
            return false;
            //return state.is_auth;
        },
        getId:state=>{return state.user.id}
    }
}
