import Vue from 'vue';
import scale from '../services/ScaleService';

export default {
    namespaced: true,
    state: {
        is_game:false,
        is_gameScene:false,
        game:{},
        timer_cd_gun:0,
        last_timer:0,
        add_bullets:[],
        del_bullets:[],
        time_to_start: 1000*30,
        size:{w:0,h:0}
    },
    mutations: {
        init(state,data){
            console.log('получил игру от сервера = ', data);
            state.game = data;
        },

        setGame(state, data){
            
            state.game = data;
        },

        addBullet(state, data){
            state.add_bullets.push(data);
        },

        delBullet(state, data){
            state.del_bullets.push(data);
        },

        setAddBullets(state, data){
            state.add_bullets = data;
        },

        setDelBullets(state, data){
            state.del_bullets = data;
        },

        delGame(state){
            state.is_game = false;
            state.is_gameScene = false;
            state.game = {};
        },

        updateGame(state, data){
            //console.log('принял = ', data);
            for (const key in data) {
                //console.log('key = ', key);
                Vue.set(state.game, key, data[key]);
                //state.game[key] = data[key];
            }
            //state.game = data;
        },
        isGame(state, data){
            state.is_game = data;
        },
        isGameScene(state, data){
            state.is_gameScene = data;
        },

        updateTimerCdGun(state, data){  
            state.timer_cd_gun = data;
            //const t_p = state.timer_cd_gun * 100 /(3*60);
            //state.last_timer = state.timer_cd_gun;
            //const t_p = state.timer_cd_gun * 100 /(3*60);
            //const p = 100-t_p-state.percent_cd_gun;
            //state.percent_cd_gun += 0.1;
        }
    },
    actions: {
        onResize({state},data){
            const size = scale.getSize(data.w,data.h);
            if(data.w<data.h){
                //console.log("должен повернуть!!!");
                
                size['rot'] = 90;
            }
            state.size = size;
            console.log('state.size = ', state.size);
            //state.action_queue.push({action:'resize',data:size});
            return Promise.resolve(size);
        }
    },
    getters: {
        getTankPos:state=>{
            return {
                x:state.game.player.x,
                y:state.game.player.y
            }
        },
        getTowerAngle:state=>{
            if(state.game && state.game.player){
                let angle = state.game.player.tower_angle;

                return angle+state.game.player.angle;
            }
            return 0;
        },
        isGame:state=>{
            return state.is_game;
        },
        getTimeToStart:state=>{
            if(!state.game){
                return 0;
            }
            //console.log(state.time_to_start,'||',state.game.time);
            let time_mil = state.time_to_start - state.game.time;
            let sec = time_mil/1000;
            return Math.floor(sec);
        },
        getTimerCdGun:state=>{
            if(state.timer_cd_gun>0){
                let str = '';
                let fullStr = String(state.timer_cd_gun/60);
                let sec = fullStr.slice(0, 1);
                str+=sec;
                let mil = fullStr.slice(2, 4);
                //console.log('mil = ', mil);
                if(mil.length>0){
                    str+=`:${mil}`;
                }else{
                    str+=':00';
                }
                return str;
            }
        },
        getPercentCdGun:state=>{
            if(state.timer_cd_gun <= 0){
                return 125;
            }
            const t_p = state.timer_cd_gun * 100 /(3*60);
            const t_val = 125*(100-t_p)/100;
            const value = 125-t_val;
            //200 ⋅ 100 : 1000 = 20
            //let t_p = state.timer_cd_gun * 100 /(3*60);
            //console.log(100-t_p);
            return value;
        },
        getPercentPlayerHp:state=>{
            if(!state.game){
                return -1;
            }

            if(!state.game.player){
                return -1;
            }
            //console.log('state.game.player = ', state.game.player);
            //const percent = state.game.player.hit_points*100/state.game.player.max_hit_points
            return state.game.player.percent_hp;
        }
    }
}
