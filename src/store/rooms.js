export default {
    namespaced: true,
    state: {
        rooms:null,
        room:null
    },
    mutations: {
        setRooms(state, rooms){
            state.rooms = rooms;
        },
        updateRoom(state, room){
            state.room = room;
            console.log('room = ', room);
        }
    },
    actions: {
       
    },
    getters: {
        getRooms:state=>{
            return state.rooms;
        },
        getRoom:state=>{
            return state.room; 
        },
        getPlayerName:(state, getters, globState ,globGetters)=>player=>{
            console.log('getters = ',globGetters);
            console.log(player.id+'|'+globGetters['user/getId']);
            if(player.id==globGetters['user/getId']){
                return "Вы";
            }
            return player.name;
        }
    }
}
