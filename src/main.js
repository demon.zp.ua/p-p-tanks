import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import {socket} from './services/socketServices';

Vue.config.productionTip = false;

//const socket = 



Vue.prototype.$socket = socket;

const vue = new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app');

export default vue;